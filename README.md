# LEARNING DATA ANALYSIS & DATA VIZ with Me

My name's *Is2ac*, data enthousiast, data lover, data fullstack learner.
On this gitlab repository, let's learn together how we can use data and pricisely data analysis tools to process data,
and create crucial insights for helping decision making.

On this repo, you'll see project with a lot of tools of data viz like : 
- POWER BI
- GRAFANA
- LOOKER, etc ...


Stay focus, and let's learn together ! 

-----------------------------------------------------------------------------------------

Je m'appelle *Is2ac*, je suis un data enthousiast, data lover, data fullstack learner.
Sur ce dépôt gitlab, apprenons ensemble comment nous pouvons utiliser les données et les outils d'analyse de données pour traiter les données,
et créer des informations cruciales pour aider à la prise de décision.

Sur ce dépôt, vous verrez un projet avec beaucoup d'outils d'analyse de données comme : 
- POWER BI
- GRAFANA
- LOOKER, etc ...


Restez concentré, et apprenons ensemble !