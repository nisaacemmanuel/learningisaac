This dashboard developed in Power BI is part of an in-depth analysis of human resources data, providing a strategic and holistic view of the various key aspects of human capital management within the organisation.

This project is the first in a long series focused on interactive visualisation. This dashboard provides a user-friendly interface enabling HR managers and decision-makers to make informed decisions. The main features of this dashboard include an in-depth analysis of the workforce, a visualisation of the KPIs (Key Performance Indicators) associated with the services and sales achieved.

The aim is to help you make the right decisions when it comes to managing your human capital!

We would like to thank you ...

----------------------------------------------------------------------------------------------------------------------------------------------

Ce tableau de bord élaboré dans Power BI s'inscrit dans le cadre d'une analyse approfondie des données liées aux ressources humaines, offrant une vision stratégique et holistique de divers aspects clés de la gestion du capital humain au sein de l'organisation.

Ce projet est le premier d'une longue serie mettant l'accent sur la visualisation interactive. Ce dashboard offre une interface conviviale permettant aux responsables des ressources humaines et aux décideurs de prendre des décisions éclairées. Les principales fonctionnalités de ce tableau de bord incluent une analyse approfondie des effectifs, un visuel sur les KPI(Key Performance Indicator) associés aux services, et aux ventes realisées.

L'objectif etant de principalement orienté correctement les prises de decision liées à la gestion du capital humain !

Merci ...